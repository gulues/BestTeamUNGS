package vistas;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

import controlador.Coordinador;
import controlador.setDatosRandom;
import modelo.Jugador;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JTabbedPane;
import javax.swing.ImageIcon;

import java.awt.SystemColor;
import java.io.File;
import java.awt.Panel;

import javax.swing.JSeparator;

import java.awt.Font;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class VistaPrincipal extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable tblJugadores;
	public JFrame frame;

	private JTabbedPane tabbedPane;
	public static DefaultTableModel modeloJugadores;
	public static JButton btnAgregarJugador;
	private JSeparator separator;
	private Coordinador miCoordinador;
	private static JLabel lblBeneficioPromedio;
	private static JLabel lbtotalJugarores;
	static protected Set<String> selecciones;
	static protected ArrayList<modelo.posiciones> posiciones;
	public static ArrayList<Jugador> listaJugadores = new ArrayList<Jugador>();

	static public JLabel lblInfo;

	public VistaPrincipal() {

		setTitle("Equipo Ideal");

		String path = new File("").getAbsolutePath();
		setResizable(false);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();

		}
		lblBeneficioPromedio= new JLabel("");
		lbtotalJugarores = new JLabel("Total de Registros: ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 889, 634);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.controlHighlight);
		setContentPane(contentPane);
		// Creamos objetos graficos
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		JButton btnGenerarEquipo = new JButton("Buscar Equipo Ideal");

		btnGenerarEquipo.setBounds(183, 18, 163, 57);
		JButton btnGenerar = new JButton("Generar Datos Aleatorios");

		btnGenerar.setBounds(528, 18, 163, 57);
		btnAgregarJugador = new JButton("Agregar Jugador");

		btnAgregarJugador.setBounds(10, 18, 163, 57);

		tabbedPane.setBounds(10, 123, 863, 441);
		JScrollPane contenedorTblReservas = new JScrollPane();
		//

		btnGenerar.setIcon(new ImageIcon(path + "/icons/player.png"));
		btnAgregarJugador.setIcon(new ImageIcon(path + "/icons/reserva.png"));
		btnGenerarEquipo.setIcon(new ImageIcon(path + "/icons/cancha.png"));
		contentPane.setLayout(null);

		separator = new JSeparator();
		separator.setBounds(10, 86, 863, 2);
		contentPane.add(separator);
		contentPane.add(btnGenerar);
		contentPane.add(btnAgregarJugador);
		//guarda la lista total de jugadores
		JButton btnguardarLista = new JButton("Guardar Lista");
		btnguardarLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				miCoordinador.guardar(listaJugadores);
			}
		});
		btnguardarLista.setBounds(710, 18, 163, 57);
		contentPane.add(btnguardarLista);
		
		JButton button = new JButton("Recargar Lista");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				refreshTable(listaJugadores);
			}
		});
		button.setBounds(355, 18, 163, 57);
		contentPane.add(button);
		contentPane.add(tabbedPane);
		contentPane.add(btnGenerarEquipo);

		// Crear modelo columnas de reservas
		modeloJugadores = new DefaultTableModel();
		modeloJugadores.addColumn("Nombre Jugador");
		modeloJugadores.addColumn("Pais");
		modeloJugadores.addColumn("Posicion");
		modeloJugadores.addColumn("Tarjetas");
		modeloJugadores.addColumn("Goles");
		modeloJugadores.addColumn("Faltas");
		modeloJugadores.addColumn("Puntaje");
		modeloJugadores.addColumn("Beneficio");

		selecciones = new HashSet<String>();

		//refreshTable(listaJugadores);
		lblBeneficioPromedio = new JLabel("Beneficio Promedio:");
		lblBeneficioPromedio.setBounds(10, 580, 189, 14);
		contentPane.add(lblBeneficioPromedio);

		// Acciones
		// Generar datos al azar
		btnGenerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setDatosRandom.generarEquipoRnd(listaJugadores);
				refreshTable(listaJugadores);
				lblBeneficioPromedio.setText("Beneficio Promedio: " + Math.round(beneficioPromedio()));
			}
		});
		//boton para abrir la vistaAgregarJugador
		btnAgregarJugador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				VistaAgregarJugador nuevoJugador = new VistaAgregarJugador(frame, selecciones, posiciones, null);
				nuevoJugador.setVisible(true);
			}
		});
		// EQUIPO IDEAL
		btnGenerarEquipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ArrayList<Jugador> listaIdeal = miCoordinador.buscarEquipoIdeal(listaJugadores);
				refreshTable(listaIdeal);
				lblInfo.setText("PUNTAJE ACTUAL: " + getBeneficio(listaIdeal));

			}

		});
		// Tabla Reservas
		tabbedPane.addTab("Lista de jugadores", null, contenedorTblReservas);
		contenedorTblReservas.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contenedorTblReservas.setViewportView(tblJugadores);
		tblJugadores = new JTable(modeloJugadores);
		tblJugadores.addMouseListener(new MouseAdapter() {
			@Override
			// Modificar jugador doble clic sobre la lista
			public void mouseClicked(MouseEvent e) {
				//si le doy doble clic a la vista
				if (e.getClickCount() == 2) {
					int id = tblJugadores.getSelectedRow();
					VistaAgregarJugador jugadorSelect = new VistaAgregarJugador(frame, selecciones, listaJugadores.get(id).getPosicion(),
							listaJugadores.get(id));
					jugadorSelect.setVisible(true);
				}
			}
		});
		tblJugadores.setFillsViewportHeight(true);
		contenedorTblReservas.setViewportView(tblJugadores);
		Panel panel = new Panel();
		panel.setBounds(10, 86, 863, 31);
		contentPane.add(panel);
		panel.setLayout(null);

		lblInfo = new JLabel("");
		lblInfo.setForeground(Color.RED);
		lblInfo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblInfo.setBounds(0, 11, 382, 14);
		panel.add(lblInfo);
		
		lbtotalJugarores.setBounds(10, 565, 326, 14);
		contentPane.add(lbtotalJugarores);

		// Deshabilitar edicion de tablas
		for (int c = 0; c < tblJugadores.getColumnCount(); c++) {
			Class<?> col_class = tblJugadores.getColumnClass(c);
			tblJugadores.setDefaultEditor(col_class, null); // remove editor
		}

	}

	protected String getBeneficio(ArrayList<Jugador> lista) {
		double totalBeneficio=0;
		for (Jugador jugador : lista) {
			totalBeneficio= totalBeneficio+ jugador.getBeneficio(); 
		}
		return totalBeneficio+"";
	}
	//Muestra los datos en la TABLA
	public static void refreshTable(ArrayList<Jugador> listadoJugadores) {
		// Cargar modelo de jugadores
		int jugTotal = modeloJugadores.getRowCount();

		for (int i = 0; i < jugTotal; i++)
			modeloJugadores.removeRow(0);

		String[] arreglo = new String[8];

		for (Jugador j : listadoJugadores) {
			selecciones.add(j.getPais());
			// posiciones.addAll(j.getPosicion());
			arreglo[0] = j.getNombre();
			arreglo[1] = j.getPais();
			String pos=j.getPosicion().toString();
			pos=pos.replace("[", "");pos=pos.replace("]", "");
			arreglo[2] = pos;
			arreglo[3] = j.getTarjetas() + "";
			arreglo[4] = j.getGoles() + "";
			arreglo[5] = j.getFaltas() + "";
			arreglo[6] = j.getPuntaje() + "";
			arreglo[7] = j.getBeneficio() + "";
			modeloJugadores.addRow(arreglo);
		}
		lbtotalJugarores.setText("Total Jugadores: "+ modeloJugadores.getRowCount()); 
		lblBeneficioPromedio.setText("Beneficio Promedio: " +  Math.round(beneficioPromedio()));
		
	}

	public static double beneficioPromedio() {
		double beneficioProm = 0;
		for (Jugador jugador : listaJugadores) {
			beneficioProm = beneficioProm + jugador.getBeneficio();
		}
		return beneficioProm / listaJugadores.size();
	}

	public static void agregarATabla() {
		refreshTable(listaJugadores);

	}

	public void setLista(ArrayList<Jugador> lista) {
		listaJugadores = lista;
		refreshTable(listaJugadores);
	}

	public void setCoordinador(Coordinador miCoordinador) {
		this.miCoordinador= miCoordinador;
	}
}