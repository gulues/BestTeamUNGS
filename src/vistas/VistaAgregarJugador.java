package vistas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import modelo.Jugador;
import modelo.posiciones;
import java.awt.Component;
import java.awt.Color;
import javax.swing.JCheckBox;

public class VistaAgregarJugador extends JDialog {
	private static final long serialVersionUID = -4754671575152617996L;

	private JButton btnAceptar;
	private Jugador nuevoJugador;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JComboBox<String> cmbSeleccion;
	private JComboBox<String> cmbTarjetas;
	private JComboBox<String> cmbGoles;
	private JComboBox<String> cmbFaltas;
	private JComboBox<String> cmbPuntaje;
	private JLabel lblBeneficio;
	private String nombre;
	protected String pais;
	private static ArrayList<posiciones> posicion;
	private int tarjetas;
	private int goles;
	private int faltas;
	private int puntaje;
	private double beneficio;

	public VistaAgregarJugador(JFrame framePadre, Set<String> selecciones, ArrayList<posiciones> posiciones,
			Jugador jugador) {
		super(framePadre, true);
		inicializarPantalla();
		agregarPanel();
		agregarElementos(selecciones, posiciones, jugador);
	}

	private void inicializarPantalla() {
		setTitle("Agregar Jugador");
		setResizable(false);
		setBounds(0, -12, 488, 366);
		setLocationRelativeTo(null);

	}

	private void agregarPanel() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}

	private void agregarElementos(Set<String> selecciones, final ArrayList<posiciones> posiciones, final Jugador jugador) {
		
		if (posiciones==null || posiciones.isEmpty())
			//si esta agregando nuevo jugador
			posicion = new ArrayList<posiciones>();
		else
			//si esta modificando jugador
			posicion= posiciones;
			
		JLabel lblFoto = new JLabel(new ImageIcon("img\\player.png"));
		lblFoto.setBounds(332, 0, 150, 150);
		contentPane.add(lblFoto);
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(24, 11, 75, 20);
		contentPane.add(lblNombre);
		txtNombre = new JTextField();
		txtNombre.setEditable(true);
		txtNombre.setColumns(10);
		txtNombre.setBounds(97, 10, 200, 20);
		txtNombre.setToolTipText("Ingrese un nombre");
		contentPane.add(txtNombre);

		JLabel lblSeleccion = new JLabel("Seleccion:");
		lblSeleccion.setBounds(24, 42, 117, 20);
		contentPane.add(lblSeleccion);

		cmbSeleccion = new JComboBox<String>();

		cmbSeleccion.addItem("");
		for (String seleccion : selecciones) {
			cmbSeleccion.addItem(seleccion);
		}
		cmbSeleccion.setBounds(97, 40, 130, 20);
		contentPane.add(cmbSeleccion);

		JLabel lblPos = new JLabel("Posiciones:");
		lblPos.setBounds(24, 75, 75, 20);
		contentPane.add(lblPos);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		btnAceptar.setEnabled(false);

		JLabel lblTarjetas = new JLabel("Tarjetas:");
		lblTarjetas.setBounds(24, 127, 75, 20);
		contentPane.add(lblTarjetas);

		cmbTarjetas = new JComboBox<String>();
		cmbTarjetas.setBounds(97, 127, 93, 20);
		contentPane.add(cmbTarjetas);

		JLabel lblGoles = new JLabel("Goles:");
		lblGoles.setBounds(24, 162, 75, 20);
		contentPane.add(lblGoles);

		cmbGoles = new JComboBox<String>();
		cmbGoles.setEditable(false);
		cmbGoles.setBounds(97, 162, 93, 20);
		contentPane.add(cmbGoles);

		JLabel lblFaltas = new JLabel("Faltas:");
		lblFaltas.setBounds(24, 197, 75, 20);
		contentPane.add(lblFaltas);

		cmbFaltas = new JComboBox<String>();
		cmbFaltas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				habilitarBotonAceptar();
			}
		});
		cmbFaltas.setBounds(97, 197, 93, 20);
		contentPane.add(cmbFaltas);
		btnAceptar.setBounds(97, 281, 130, 45);
		contentPane.add(btnAceptar);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setAlignmentY(Component.BOTTOM_ALIGNMENT);

		btnCancelar.setBounds(241, 281, 130, 45);
		contentPane.add(btnCancelar);

		JLabel lblPuntaje = new JLabel("Puntaje:");
		lblPuntaje.setBounds(24, 232, 75, 20);
		contentPane.add(lblPuntaje);
		cmbPuntaje = new JComboBox<String>();
		cmbPuntaje.setBounds(97, 232, 93, 20);
		contentPane.add(cmbPuntaje);

		lblBeneficio = new JLabel("Beneficio:");
		lblBeneficio.setForeground(Color.RED);
		// AR,LI,LD,CI,CD,VI,VC,VD,PI,DC,PD
		final JCheckBox chkAr = new JCheckBox("AR");
		chkAr.setToolTipText("Arquero");
		chkAr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setPosiciones(chkAr.getText(), chkAr.isSelected());
			}
		});
		chkAr.setBounds(98, 75, 39, 20);
		contentPane.add(chkAr);
		final JCheckBox chkLi = new JCheckBox("LI");
		chkLi.setToolTipText("Lateral Izquierdo");
		chkLi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPosiciones(chkLi.getText(), chkLi.isSelected());
			}
		});
		chkLi.setBounds(133, 75, 39, 20);
		contentPane.add(chkLi);
		final JCheckBox chkLd = new JCheckBox("LD");
		chkLd.setToolTipText("Lateral Derecho");
		chkLd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPosiciones(chkLd.getText(), chkLd.isSelected());
			}
		});
		chkLd.setBounds(168, 75, 39, 20);
		contentPane.add(chkLd);
		final JCheckBox chkCi = new JCheckBox("CI");
		chkCi.setToolTipText("Central Izquierdo");
		chkCi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPosiciones(chkCi.getText(), chkCi.isSelected());
			}
		});
		chkCi.setBounds(203, 75, 39, 20);
		contentPane.add(chkCi);
		final JCheckBox chkCd = new JCheckBox("CD");
		chkCd.setToolTipText("Central Derecho");
		chkCd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPosiciones(chkCd.getText(), chkCd.isSelected());
			}
		});
		chkCd.setBounds(238, 75, 39, 20);
		contentPane.add(chkCd);
		final JCheckBox chkVi = new JCheckBox("VI");
		chkVi.setToolTipText("Volante Izquierdo");
		chkVi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPosiciones(chkVi.getText(), chkVi.isSelected());
			}
		});
		chkVi.setBounds(273, 75, 39, 20);
		contentPane.add(chkVi);
		final JCheckBox chkVc = new JCheckBox("VC");
		chkVc.setToolTipText("Volante Central");
		chkVc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPosiciones(chkVc.getText(), chkVc.isSelected());
			}
		});
		chkVc.setBounds(98, 100, 39, 20);
		contentPane.add(chkVc);
		final JCheckBox chkVd = new JCheckBox("VD");
		chkVd.setToolTipText("Volante Derecho");
		chkVd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPosiciones(chkVd.getText(), chkVd.isSelected());
			}
		});
		chkVd.setBounds(133, 100, 39, 20);
		contentPane.add(chkVd);
		final JCheckBox chkPi = new JCheckBox("PI");
		chkPi.setToolTipText("Puntero Izquierdo");
		chkPi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPosiciones(chkPi.getText(), chkPi.isSelected());
			}
		});
		chkPi.setBounds(168, 100, 39, 20);
		contentPane.add(chkPi);
		final JCheckBox chkDc = new JCheckBox("DC");
		chkDc.setToolTipText("Central Delantero");
		chkDc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPosiciones(chkDc.getText(), chkDc.isSelected());
			}
		});
		chkDc.setBounds(203, 100, 39, 20);
		contentPane.add(chkDc);
		final JCheckBox chkPd = new JCheckBox("PD");
		chkPd.setToolTipText("Puntero Derecho");
		chkPd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPosiciones(chkPd.getText(), chkPd.isSelected());
			}
		});
		chkPd.setBounds(238, 100, 39, 20);
		contentPane.add(chkPd);

		// contentPane
		cmbTarjetas.addItem("");
		cmbGoles.addItem("");
		cmbFaltas.addItem("");
		cmbPuntaje.addItem("");

		for (int i = 0; i < 100; i++) {
			cmbGoles.addItem(i + "");
			cmbTarjetas.addItem(i + "");
			cmbFaltas.addItem(i + "");
			cmbPuntaje.addItem(i + "");
		}
		if (jugador != null) {// modifica jugador
			btnAceptar.setText("Modificar");
			txtNombre.setText(jugador.getNombre());
			txtNombre.setEnabled(false);
			cmbSeleccion.setSelectedItem(jugador.getPais());
			cmbSeleccion.setEnabled(false);
			cmbTarjetas.setSelectedItem(jugador.getTarjetas() + "");
			cmbGoles.setSelectedItem(jugador.getGoles() + "");
			cmbFaltas.setSelectedItem(jugador.getFaltas() + "");
			cmbPuntaje.setSelectedItem(jugador.getPuntaje() + "");
			lblBeneficio.setText("Beneficio: " + jugador.getBeneficio());
		}
		// ActionListeners
		txtNombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				habilitarBotonAceptar();
			}
		});
		cmbSeleccion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				habilitarBotonAceptar();
			}
		});
		cmbTarjetas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				habilitarBotonAceptar();
			}
		});
		cmbGoles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				habilitarBotonAceptar();
			}
		});
		cmbPuntaje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				habilitarBotonAceptar();
			}
		});
		cmbTarjetas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				habilitarBotonAceptar();
			}
		});

		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnAceptar.addActionListener(new ActionListener() {
		//boton agregar-modificar jugador	
			
			public void actionPerformed(ActionEvent arg0) {
				nombre = txtNombre.getText();
				pais = (String) cmbSeleccion.getSelectedItem().toString();
				tarjetas = Integer.parseInt(cmbTarjetas.getSelectedItem().toString());
				goles = Integer.parseInt(cmbGoles.getSelectedItem().toString());
				faltas = Integer.parseInt(cmbFaltas.getSelectedItem().toString());
				puntaje = Integer.parseInt(cmbPuntaje.getSelectedItem().toString());
				nuevoJugador = new Jugador(nombre, pais, posicion, tarjetas, goles, faltas, puntaje, beneficio);
				if (btnAceptar.getText().equals("Aceptar")) {
					VistaPrincipal.listaJugadores.add(nuevoJugador);
				} else {
					jugador.setBeneficio(beneficio);
					jugador.setFaltas(faltas);
					jugador.setGoles(goles);
					jugador.setNombre(nombre);
					jugador.setTarjetas(tarjetas);
					jugador.setPais(pais);
					jugador.setPosicion(posicion);
					jugador.setPuntaje(puntaje);
					jugador.setTarjetas(tarjetas);

				}
				VistaPrincipal.agregarATabla();
				dispose();
			}
		});
		if (!(posiciones == null))
			for (posiciones pos : posiciones) {
				if (pos.toString().equals("AR"))
					chkAr.setSelected(true);
				if (pos.toString().equals("CD"))
					chkCd.setSelected(true);
				if (pos.toString().equals("CI"))
					chkCi.setSelected(true);
				if (pos.toString().equals("DC"))
					chkDc.setSelected(true);
				if (pos.toString().equals("LD"))
					chkLd.setSelected(true);
				if (pos.toString().equals("LI"))
					chkLi.setSelected(true);
				if (pos.toString().equals("PI"))
					chkPi.setSelected(true);
				if (pos.toString().equals("VC"))
					chkVc.setSelected(true);
				if (pos.toString().equals("VD"))
					chkVd.setSelected(true);
				if (pos.toString().equals("VI"))
					chkVi.setSelected(true);

			}

	}

	protected void setPosiciones(String text, boolean selected) {
		if (selected)
			posicion.add(posiciones.valueOf(text));
		else
			posicion.remove(posiciones.valueOf(text));
		if (posicion.size() == 0)
			btnAceptar.setEnabled(false);
		else
			habilitarBotonAceptar();
	}

	private void habilitarBotonAceptar() {
		if (!(txtNombre.getText().length() < 4 || cmbGoles.getSelectedIndex() == 0
				|| cmbTarjetas.getSelectedIndex() == 0 || cmbSeleccion.getSelectedIndex() == 0
				|| cmbFaltas.getSelectedIndex() == 0 || cmbPuntaje.getSelectedIndex() == 0)) {
			btnAceptar.setEnabled(true);
			try {
				tarjetas = Integer.parseInt(cmbTarjetas.getSelectedItem().toString());
				goles = Integer.parseInt(cmbGoles.getSelectedItem().toString());
				faltas = Integer.parseInt(cmbFaltas.getSelectedItem().toString());
				puntaje = Integer.parseInt(cmbPuntaje.getSelectedItem().toString());
				beneficio = ((2 * goles) - ((faltas / 10)) - tarjetas) + puntaje;
				lblBeneficio.setText("beneficio: " + beneficio);
			} catch (Exception e) {
				// TODO: handle exception
			}

		} else
			btnAceptar.setEnabled(false);
	}
}
