package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class equipoIdeal {

	private static int tarjetas;
	private static int seleccion;
	private static int goles;
	private static ArrayList<Jugador> listaIdeal;
	private static ArrayList<Jugador> listaTotalJugadores;
	private static ArrayList<posiciones> listPosiciones;

	public static ArrayList<Jugador> getEquipoIdeal(ArrayList<Jugador> listaTotal) {
		//reinicio valores
		tarjetas=0;goles=0;seleccion=0;
		listPosiciones = new ArrayList<posiciones>();
		for (posiciones posicion : posiciones.values()) {
			listPosiciones.add(posicion);
		}
		setListaTotalJugadores(listaTotal);
		listaIdeal = new ArrayList<Jugador>();
		// ordeno por beneficio
		ordenarBeneficio(listaTotal);
		// listaIdeal= mejores 11
		listaIdeal = mejorEquipo(listaTotal);
		//ordenarPosicion(listaIdeal);
		mejorBeneficio(listaIdeal);

		return listaIdeal;

	}

	private static void ordenarBeneficio(ArrayList<Jugador> listaTotal) {
		Collections.sort(listaTotal, new Comparator<Jugador>() {
			public int compare(Jugador a, Jugador b) {
				if (a.getBeneficio() == b.getBeneficio())
					return a.getNombre().compareTo(b.getNombre());
				return a.getBeneficio() < b.getBeneficio() ? 1 : a.getBeneficio() > b.getBeneficio() ? -1 : 0;
			}
		});
	}

	// ordenamiento de de la lista por mejor puntaje
	private static ArrayList<Jugador> mejorEquipo(ArrayList<Jugador> listaTotal) {
		// busca en la lista los jugadores que cumplen con el
		// criterio y los agrega en la listaIdeal
		for (Jugador jugador : listaTotal) {
			if (cumpleCriterios(jugador))
				listaIdeal.add(jugador);
		}

		return listaIdeal;
	}

	private static double mejorBeneficio(ArrayList<Jugador> lj) {
		double beneficio = 0;
		for (Jugador jugador : lj)
			beneficio = beneficio + jugador.getBeneficio();
		return beneficio;

	}

	// 1. Se debe tener exactamente un jugador en cada una de las posiciones
	// listadas mas arriba.
	// 2. No se pueden tener mas de cuatro jugadores de una misma seleccion.
	// 3. No se pueden tener mas de cinco jugadores que hayan recibido tarjetas.
	// 4. No se pueden tener mas de seis jugadores que no hayan hecho goles.


	private static boolean cumpleCriterios(Jugador j) {
		if (j.getTarjetas() > 0) {
			if (tarjetas == 5)
				return false;
			tarjetas++;
		}
		for (Jugador jugador : listaIdeal) {
			if (jugador.getPais().equals(j.getPais())) {
				if (seleccion == 4)
					return false;
				seleccion++;
			}	
		}
		
		if (j.getGoles() == 0) {
			if (goles == 6)
				return false;
			goles++;
		}
		boolean cumple =analizarPosicion(j);
		return cumple;

	}
	// Comparo las posiciones del jugador con una lista de posiciones
	public static boolean analizarPosicion(Jugador jugador) {
		if (listPosiciones.size() != 0)
			for (posiciones posiciones : jugador.getPosicion()) {
				if (listPosiciones.contains(posiciones)) {
					listPosiciones.remove(posiciones);
					ArrayList<posiciones> listaComparablePosiciones = new ArrayList<posiciones>();
					listaComparablePosiciones.add(posiciones);
					jugador.setPosicion(listaComparablePosiciones);
					return true;
				}
			}
		return false;
	}

	public static ArrayList<Jugador> getListaTotalJugadores() {
		return listaTotalJugadores;
	}

	public static void setListaTotalJugadores(ArrayList<Jugador> listaTotalJugadores) {
		equipoIdeal.listaTotalJugadores = listaTotalJugadores;
	}

}
