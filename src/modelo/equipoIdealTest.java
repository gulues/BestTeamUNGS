package modelo;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;

public class equipoIdealTest {
private	ArrayList<Jugador> listaIdealTest;
private	Jugador nuevo;
	
	@Before
	public void setUp() throws Exception {
		ArrayList<posiciones> pos = new ArrayList<posiciones>();
		listaIdealTest = new ArrayList<>();
		int x=0;
		for (posiciones posicion : posiciones.values()) {
			pos.add(posicion);
			@SuppressWarnings("unchecked")
			Jugador j = new Jugador("nombre", "pais"+x, (ArrayList<posiciones>) pos.clone(), 0, 0, 0, 0, 0);
			x++;
			listaIdealTest.add(j);
			pos.remove(posicion);
		}
		setDatosenCero();// todo a cero

	}
	private void setDatosenCero() {
		for (Jugador jugador : listaIdealTest) {
			jugador.setBeneficio(0);
			jugador.setFaltas(0);
			jugador.setTarjetas(0);
			jugador.setGoles(1);
		}
	}
	
	@Test
	public void testEquipoIdeal() {
		ArrayList<posiciones> posicionAR= new ArrayList<posiciones>();
		posicionAR.add(posiciones.AR);
		nuevo = new Jugador("nombre", "pais", posicionAR, 0, 0, 0, 0, 10);
		//agrego un jugador a la lista con el mejor beneficio
		listaIdealTest.add(nuevo);
		listaIdealTest = equipoIdeal.getEquipoIdeal(listaIdealTest);
		assertTrue(listaIdealTest.contains(nuevo));
	}

	@Test
	public void testCriteriosPosicionRepetida() {
		ArrayList<posiciones> posicionAR= new ArrayList<posiciones>();
		posicionAR.add(posiciones.AR);
		nuevo = new Jugador("nombre", "pais", posicionAR, 0, 0, 0, 0, 0);
		listaIdealTest.remove(10);
		listaIdealTest.add(nuevo);
		ArrayList<Jugador> lista = equipoIdeal.getEquipoIdeal(listaIdealTest);
		assertTrue(!lista.contains(nuevo));
	}

	@Test
	public void testCriteriosTarjetas() {
		agregarJugadorconTarjeta();
		ArrayList<posiciones> posicionPD= new ArrayList<posiciones>();
		posicionPD.add(posiciones.PD);
		nuevo = new Jugador("nombre", "pais", posicionPD, 1, 0, 0, 0, 0);
		listaIdealTest.add(nuevo);
		ArrayList<Jugador> lista = equipoIdeal.getEquipoIdeal(listaIdealTest);
		assertTrue(!lista.contains(nuevo));
	}
	// agrego 5 jugadores con tarjetas y el resto sin 
	// quito el ultimo sin tarjeta
	private void agregarJugadorconTarjeta() {
		int tarjeta = 1;
		int cont = 0;
		for (Jugador jugador : listaIdealTest) {
			jugador.setTarjetas(tarjeta);
			if (cont > 4)
				tarjeta = 0;
			cont++;
		}
		//remuevo el ultimo para poder ingresar otro jugador con 0 goles
		listaIdealTest.remove(10);

	}
	@Test
	public void testCriteriosGoles() {
		agregarJugadorSinGoles();
		ArrayList<posiciones> posicionPD= new ArrayList<posiciones>();
		posicionPD.add(posiciones.PD);
		nuevo = new Jugador("nombre", "pais", posicionPD, 0, 0, 0, 0, 0);
		listaIdealTest.add(nuevo);
		ArrayList<Jugador> lista = equipoIdeal.getEquipoIdeal(listaIdealTest);
		assertTrue(!lista.contains(nuevo));
	}

	// agrega 5 jugadores con 0 goles y 6 jugadores con 1 gol
	// quito el utlimo que tiene un gol con posicion PD
	private void agregarJugadorSinGoles() {
		int goles = 0;
		int cont = 0;
		for (Jugador jugador : listaIdealTest) {
			jugador.setGoles(goles);
			if (cont > 5)
				goles = 1;
			cont++;
		}
		//remuevo el ultimo para poder ingresar otro jugador con 0 goles
		listaIdealTest.remove(10);

	}

	@Test
	public void testCriteriosPaises() {
		ArrayList<posiciones> posicionPD= new ArrayList<posiciones>();
		posicionPD.add(posiciones.PD);
		agregarJugadorMismoPais();
		nuevo = new Jugador("nombre", "PaisIgual", posicionPD, 0, 0, 0, 0, 0);
		//remuevo el jugador posicion PD
		listaIdealTest.remove(10);
		listaIdealTest.add(nuevo);
		ArrayList<Jugador> lista = equipoIdeal.getEquipoIdeal(listaIdealTest);
		assertTrue(!lista.contains(nuevo));
	}
	private void agregarJugadorMismoPais() {
		for (int i = 0; i < 4; i++) {
			listaIdealTest.get(i).setPais("PaisIgual");
			
		}
		
	}

}
