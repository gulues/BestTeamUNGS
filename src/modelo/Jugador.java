package modelo;
import java.io.Serializable;
import java.util.ArrayList;

public class Jugador implements Serializable {
	private static final long serialVersionUID = 1L;
	private String nombre;
	private String pais;
	private ArrayList<posiciones> posicion;
	private int tarjetas;
	private int goles;
	private int faltas;
	private int puntaje;
	private double beneficio;

	public Jugador(String nombre, String pais, ArrayList<posiciones> posicion, int tarjetas, int goles, int faltas, int puntaje, double beneficio) {
		this.nombre = nombre;
		this.pais = pais;
		this.posicion = posicion;
		this.tarjetas = tarjetas;
		this.goles = goles;
		this.faltas = faltas;
		this.puntaje = puntaje;
		this.beneficio=beneficio;
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public ArrayList<posiciones> getPosicion() {
		return posicion;
	}

	public void setPosicion(ArrayList<posiciones> posicion) {
		this.posicion = posicion;
	}

	public int getTarjetas() {
		return tarjetas;
	}

	public void setTarjetas(int tarjetas) {
		this.tarjetas = tarjetas;
	}

	public int getGoles() {
		return goles;
	}

	public void setGoles(int goles) {
		this.goles = goles;
	}

	public int getFaltas() {
		return faltas;
	}

	public void setFaltas(int faltas) {
		this.faltas = faltas;
	}

	public int getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}

	public double getBeneficio() {
		return beneficio;
	}

	public void setBeneficio(double beneficio) {
		this.beneficio = beneficio;
	}

	public void setData(Jugador nuevoJugador) {
		this.beneficio= nuevoJugador.beneficio;
		this.faltas= nuevoJugador.faltas;
		this.goles= nuevoJugador.goles;
		this.posicion= nuevoJugador.posicion;
		this.tarjetas= nuevoJugador.tarjetas;
		
	}
}
