package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import controlador.setDatosRandom;

public class archivo {
	private ArrayList<Jugador> datos;
	private final static String path = "db.txt";
	
	public void guardar(ArrayList<Jugador> lista) {
		try {
			FileOutputStream fos = new FileOutputStream(path, false);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(lista);
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	// Abrir Serializable Arraylist Jugadores con puntaje.

	@SuppressWarnings("unchecked")
	public ArrayList<Jugador> abrir() {
		try {

			FileInputStream fis = new FileInputStream(path);
			ObjectInputStream in = new ObjectInputStream(fis);
			datos = (ArrayList<modelo.Jugador>) in.readObject();
			in.close();
			fis.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return datos;
	}

	public ArrayList<Jugador> getDatos() {
		abrir();
		return datos;
	}

	// para leer desde archivo txt, primer uso! o para editar el txt facilmente
	public ArrayList<Jugador> leerArchivoTxt(String pathTxt) {
		ArrayList<Jugador> listaJugadores = new ArrayList<Jugador>();
		try {
			File fileDir = new File(pathTxt);
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF8"));
			String str;
			@SuppressWarnings("unused")
			int cont = 0;
			String pais = null;
			String[] pos_nomb = null;

			while ((str = in.readLine()) != null) {
				cont++;
				pos_nomb = str.split(",");
				if (pos_nomb.length == 1)// lee en archivo el pais
					pais = pos_nomb[0];
				if (pos_nomb.length == 2) {// lee en archivo posicion,nombre
					// genero randoms para tarjeta, goles, faltas, puntaje, beneficio;
					String[] arrpos = pos_nomb[0].split(";");
					ArrayList<posiciones> posicion = new ArrayList<>();
					for (String string : arrpos)
						posicion.add(posiciones.valueOf(string));
					String nombre = pos_nomb[1];
					Jugador jugadorRnd = new Jugador(nombre, pais, posicion, 0, 0, 0, 0, 0);
					setDatosRandom.generarJugadorRnd(jugadorRnd);
					listaJugadores.add(jugadorRnd);
				}

			}
			in.close();
		} catch (IOException e) {
			System.out.println("Error En archivo");
		}
		//guardar(listaJugadores);
		return listaJugadores;

	}

}
