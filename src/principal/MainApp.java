package principal;

import controlador.Coordinador;
import vistas.VistaPrincipal;

public class MainApp {

	public static void main(String[] args) {
		
		VistaPrincipal ventanaPrincipal = new VistaPrincipal();
		ventanaPrincipal.setLocationRelativeTo(null);
		Coordinador miCoordinador = new Coordinador();
		ventanaPrincipal.setCoordinador(miCoordinador);
		ventanaPrincipal.setLista(miCoordinador.getLista());
		ventanaPrincipal.setVisible(true);
	}

}
