package controlador;
import java.util.ArrayList;
import java.util.Random;
//Genera Datos Aleatorios
//Obtiene Pais,Nombre,Posicion de TXT

import modelo.Jugador;

public class setDatosRandom {
private static int maxGolesRND=15;
private static int maxTarjetasRDN=20;
private static int maxFaltasRDN=15;
private static int maxPuntajeRND=20;
	
//Genera un random de datos del jugador en tiempo real
	public static void generarEquipoRnd(ArrayList<Jugador> listaJugador) {
		for (Jugador j : listaJugador) {
			Random rand = new Random();
			int tarjetas = rand.nextInt(maxTarjetasRDN);
			int goles = rand.nextInt(maxGolesRND);
			int faltas = rand.nextInt(maxFaltasRDN);
			int puntaje = rand.nextInt(maxPuntajeRND);
			double beneficio = ((2 * goles) - ((faltas / 10)) - tarjetas) + puntaje;
			j.setTarjetas(tarjetas);
			j.setGoles(goles);
			j.setFaltas(faltas);
			j.setPuntaje(puntaje);
			j.setBeneficio(beneficio);
			
		}
	}
	
	//Metodo para setear los datos cuando toma de archivo txt
	public static void generarJugadorRnd(Jugador jugador) {
			
			Random rand = new Random();
			int tarjetas = rand.nextInt(maxTarjetasRDN);
			int goles = rand.nextInt(maxGolesRND);
			int faltas = rand.nextInt(maxFaltasRDN);
			int puntaje = rand.nextInt(maxPuntajeRND);
			double beneficio = (2 * goles) - ((faltas / 10) - tarjetas) + puntaje;
			jugador.setTarjetas(tarjetas);
			jugador.setGoles(goles);
			jugador.setFaltas(faltas);
			jugador.setPuntaje(puntaje);
			jugador.setBeneficio(beneficio);
			
		
	}
}
