package controlador;



import java.util.ArrayList;

import modelo.Jugador;
import modelo.archivo;
import modelo.equipoIdeal;


public class Coordinador {
	
	public ArrayList<Jugador>getLista() {
		archivo file = new archivo();
		//toma los datos desde db.txt serializable
		ArrayList<Jugador> lista= file.getDatos();
		//toma los datos desde archivo para poder editar nombre, pais, posiciones de los jugadores
		//ArrayList<Jugador> lista= file.leerArchivoTxt("jugadores.txt");
		return  lista;
		
	}
	public ArrayList<Jugador> buscarEquipoIdeal(ArrayList<Jugador>listaJugadores){
		ArrayList<Jugador>listaResultado= new ArrayList<>();
		listaResultado=equipoIdeal.getEquipoIdeal(listaJugadores);
		return listaResultado;
		
	}
	
	public void guardar(ArrayList<Jugador> listaxGuardar){
		archivo a = new archivo();
		a.guardar(listaxGuardar);
		
	}
}
